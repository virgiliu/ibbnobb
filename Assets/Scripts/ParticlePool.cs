﻿using System.Collections.Generic;
using UnityEngine;

public class ParticlePool : MonoBehaviour
{
    public GameObject praticlePrefab;
    public int poolSize;

    Queue<ParticleSystem> particles = new Queue<ParticleSystem>();

    private void Awake()
    {
        GameObject particle;
        for (int idx = 0; idx < poolSize; idx++)
        {
            particle = Instantiate(praticlePrefab);
            particle.transform.SetParent(transform);
            particle.name = $"{praticlePrefab.name}_{idx}";

            particles.Enqueue(particle.GetComponent<ParticleSystem>());
        }
    }

    private ParticleSystem GetParticle()
    {
        var particle = particles.Dequeue();
        // Faux-circular queue
        particles.Enqueue(particle);

        return particle;
    }

    public void Play(Vector3 particlePosition, Color[] colors = null)
    {
        var particleSystem = GetParticle();

        particleSystem.transform.position = particlePosition;

        SetParticleColors(colors, particleSystem.main);

        // Make sure to stop the particles system first, just in case it's already playing
        particleSystem.Stop();
        particleSystem.Play();
    }

    private static void SetParticleColors(Color[] colors, ParticleSystem.MainModule particleSettings)
    {
        if (colors == null || colors.Length == 0)
        {
            particleSettings.startColor = Color.white;
        }
        else
        {
            switch (colors.Length)
            {
                case 1:
                    particleSettings.startColor = colors[0];
                    break;
                default:
                    var color = new ParticleSystem.MinMaxGradient(colors[0], colors[1]);
                    color.mode = ParticleSystemGradientMode.TwoColors;
                    particleSettings.startColor = color;
                    break;
            }
        }
    }
}
