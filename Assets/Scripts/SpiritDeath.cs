﻿using UnityEngine;

public class SpiritDeath : MonoBehaviour
{
    public ParticlePool deathParticlePool;
    public Color crawlerDeathColor = Color.black;
    public Color spiritDeathColor = Color.white;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Constants.Tags.Player))
        {
            Die();
        }
    }

    void Die()
    {
        deathParticlePool.Play(transform.position, new[] { crawlerDeathColor, spiritDeathColor });

        if (transform.parent != null)
        {
            Destroy(transform.parent.gameObject);
        }

        // TODO: Drop shiny loot, play SFX
    }
}
