﻿using UnityEngine;

public class MoveBetweenTwoPoints : MonoBehaviour
{
    public Transform leftPoint;
    public Transform rightPoint;
    public float moveSpeedMultiplier = 0.8f;

    private Vector3 leftPosition;
    private Vector3 rightPosition;

    private void Start()
    {
        // We need to store the original positions for the movement left & right limits
        // because the points will move around with the object once the game plays
        leftPosition = leftPoint.position;
        rightPosition = rightPoint.position;
    }

    private void Update()
    {
        var lerpFactor = Mathf.PingPong(Time.time * moveSpeedMultiplier, 1);
        transform.position = Vector3.Lerp(leftPosition, rightPosition, lerpFactor);
    }
}
