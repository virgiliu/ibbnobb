﻿using UnityEngine;

/// <summary>
/// Flips the player's orientation up/down and also the gravity direction when the player goes through a portal
/// </summary>
[RequireComponent(typeof(CustomPhysics))]
public class FlipPlayerAndGravityUpsideDown : MonoBehaviour
{
    private CustomPhysics customPhysics;
    private Rigidbody rb;

    private void Start()
    {
        customPhysics = GetComponent<CustomPhysics>();
        rb = GetComponent<Rigidbody>();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(Constants.Tags.Portal))
        {
            customPhysics.FlipGravity();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Constants.Tags.Portal))
        {
            // Flip player
            rb.rotation = rb.rotation * Quaternion.Euler(180, 0, 0);
        }
    }
}
