﻿public static class Constants
{
    public struct Tags
    {
        public const string Player = "Player";
        public const string Portal = "Portal";
    }

    public struct Inputs
    {
        public const string Jump = "Jump";
        public const string Horizontal = "Horizontal";
    }

    public struct Animations
    {
        public const string Idle = "Idle";
        public const string Run = "Run";
        public const string JumpUp = "JumpUp";
        public const string Falling = "Falling";
    }
}
