﻿using UnityEngine;

/// <summary>
/// Triggers death sentence for players, causing them to self destruct
/// </summary>
public class KillPlayer : MonoBehaviour
{
    public delegate void PlayerDeathSentence(GameObject whoDiesFirst);
    public static event PlayerDeathSentence OnPlayerDeathSentence;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Constants.Tags.Player))
        {
            OnPlayerDeathSentence?.Invoke(other.gameObject);
        }
    }
}
