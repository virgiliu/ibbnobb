﻿using System.Collections;
using UnityEngine;

public class PlayerDeath : MonoBehaviour
{
    public delegate void PlayerDies(GameObject player);
    public static event PlayerDies OnPlayerDies;


    // How long a player stays alive after the other player already died.
    public float deathTimer = 1.2f;
    public ParticlePool deathParticlePool;
    public Color deathColor;

    private void OnEnable()
    {
        KillPlayer.OnPlayerDeathSentence += Die;
    }

    private void OnDisable()
    {
        KillPlayer.OnPlayerDeathSentence -= Die;
    }

    private void Die(GameObject whoDiesFirst)
    {
        if (whoDiesFirst == gameObject)
        {
            StartCoroutine(ImpendingDoom(0));
        }
        else
        {
            StartCoroutine(ImpendingDoom(deathTimer));
        }
    }

    IEnumerator ImpendingDoom(float deathTimer)
    {
        yield return new WaitForSeconds(deathTimer);

        OnPlayerDies?.Invoke(gameObject);

        Destroy(gameObject);

        deathParticlePool.Play(transform.position, new[] { deathColor });

        // TODO: Play sad noises
    }
}
