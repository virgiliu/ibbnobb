﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CustomPhysics))]
[RequireComponent(typeof(SimpleInput))]
public class PlayerController : MonoBehaviour
{
    [Header("Move & jump")]
    public float moveSpeed = 3f;
    // How much to reduce horizontal speed when player is horizontally decelerated in mid-air
    public float moveSpeedAirDeceleration = 0.5f;
    public float moveSpeedMultiplierOnTopOfPlayer = 1.5f;
    public float jumpSpeed = 5f;
    // How much to increase gravity when player releases the jump key before reaching the apex of the jump
    public float bringPlayerDownGravityMultiplier = 1.4f;

    [Header("Ground detection")]
    public LayerMask groundLayer;
    public float groundCheckDistance = 4f;
    // We need at least 2 rays, one at the left and one at the right of the player, so we can stand on ledges
    public Transform[] groundCheckRaycastOrigins;

    private RaycastHit groundCheckHit;

    private Rigidbody rb;
    private CustomPhysics customPhysics;

    private SimpleInput input;
    private SimpleInput siblingInput;

    private float horizontalInput;
    private float prevHorizontalInput;
    private bool enableAirDeceleration = false;

    public bool IsGrounded { get; private set; }
    public bool IsOnTopOfPlayer { get; private set; }
    public bool IsFacingRight { get; private set; } = true;

    public bool IsFalling
    {
        get
        {
            return rb.velocity.y != 0 && OppositeSigns(rb.velocity.y, customPhysics.Gravity.y) == false;
        }
    }

    public bool IsGoingUp
    {
        get
        {
            return rb.velocity.y != 0 && OppositeSigns(rb.velocity.y, customPhysics.Gravity.y);
        }
    }

    public bool IsRunning
    {
        get
        {
            return Mathf.Abs(input.HorizontalAxis) > 0;
        }
    }

    private bool ShouldFlipDirection
    {
        get
        {
            return (input.HorizontalAxis > 0 && !IsFacingRight) || (input.HorizontalAxis < 0 && IsFacingRight);
        }
    }

    private bool ShouldJump
    {
        get
        {
            return IsGrounded && (input.WasJumpPressedThisFrame || (IsOnTopOfPlayer && siblingInput.WasJumpPressedThisFrame));
        }
    }

    private float AirDeceleration
    {
        get
        {
            if (IsGrounded)
            {
                return 1;
            }
            else
            {
                if (enableAirDeceleration)
                    return moveSpeedAirDeceleration;
                else
                    return 1;
            }
        }
    }

    private void Start()
    {
        StoreSiblingInputReference();
        rb = GetComponent<Rigidbody>();
        customPhysics = GetComponent<CustomPhysics>();
        input = GetComponent<SimpleInput>();
    }

    private void Update()
    {
        Jump();
    }

    private void FixedUpdate()
    {
        GroundCheck();
        FlipDirection();
        HorizontalMovement();
        BringPlayerDownFaster();
    }

    private void StoreSiblingInputReference()
    {
        foreach (var player in GameObject.FindGameObjectsWithTag(Constants.Tags.Player))
        {
            if (player != gameObject)
            {
                siblingInput = player.GetComponent<SimpleInput>();
                return;
            }
        }
    }

    private void Jump()
    {
        if (ShouldJump)
        {
            rb.velocity = new Vector3(
                            rb.velocity.x,
                            rb.velocity.y + jumpSpeed * Mathf.Sign(customPhysics.Gravity.y) * -1,
                            rb.velocity.z);
        }
    }

    private void HorizontalMovement()
    {
        horizontalInput = input.HorizontalAxis;

        if (IsOnTopOfPlayer)
        {
            // When player is on top of the other player and the bottom player moves, move top player as well.
            if (Mathf.Abs(horizontalInput) == 0)
            {
                horizontalInput = siblingInput.HorizontalAxis;
            }
            // Move faster when on top of the other player, otherwise if both players move in the same direction
            // the top player will be stuck on top.
            else
            {
                horizontalInput *= moveSpeedMultiplierOnTopOfPlayer;
            }
        }

        // Player released the movement button in the air: slow him down and use his last input value as the speed
        // so the movement doesn't stop abruptly.
        if (horizontalInput == 0 && IsGrounded == false)
        {
            enableAirDeceleration = true;
            rb.velocity = new Vector3(moveSpeed * prevHorizontalInput * AirDeceleration, rb.velocity.y, rb.velocity.z);
        }
        // Move normally when the player is keeping the movement button pressed during jump.
        else
        {
            rb.velocity = new Vector3(moveSpeed * horizontalInput * AirDeceleration, rb.velocity.y, rb.velocity.z);
            prevHorizontalInput = horizontalInput;
        }

    }

    private void FlipDirection()
    {
        if (!ShouldFlipDirection)
        {
            return;
        }

        IsFacingRight = !IsFacingRight;

        // If player turns around while jumping he should be slowed down.
        if (!IsGrounded)
        {
            enableAirDeceleration = true;
        }
    }

    private void GroundCheck()
    {
        foreach (var origin in groundCheckRaycastOrigins)
        {
            if (Physics.Raycast(origin.position, -origin.up, out groundCheckHit, groundCheckDistance, groundLayer))
            {
                IsGrounded = true;
                enableAirDeceleration = false;
                if (groundCheckHit.collider.CompareTag(Constants.Tags.Player))
                {
                    IsOnTopOfPlayer = true;
                }
                return;
            }
        }

        IsGrounded = false;
        IsOnTopOfPlayer = false;
    }

    // Make the player's jump height to be proportional to how long the jump button was held down.
    // This is done by applying a higher gravity when the player is going up without having the jump button pressed.
    private void BringPlayerDownFaster()
    {
        if (IsGoingUp && !input.IsJumpHeldDown && !IsOnTopOfPlayer)
        {
            rb.velocity = new Vector3(
            rb.velocity.x,
            rb.velocity.y + (customPhysics.Gravity.y * Time.deltaTime) * bringPlayerDownGravityMultiplier,
            rb.velocity.z);
        }
    }

    // TODO: this could go in an utils class, but since only this class uses the method, there's no point at this moment.
    private bool OppositeSigns(float firstValue, float secondValue)
    {
        return Mathf.Sign(firstValue) != Mathf.Sign(secondValue);
    }
}
