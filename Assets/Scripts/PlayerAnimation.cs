﻿using UnityEngine;

// Note: since the animation logic is super simple, it's faster to just control it from code 
// instead of spending time to set up transitions in the animator
[RequireComponent(typeof(PlayerController))]
public class PlayerAnimation : MonoBehaviour
{
    private Animator animator;
    private PlayerController player;
    private string currentAnimation;

    private void Start()
    {
        player = GetComponent<PlayerController>();
        animator = GetComponentInChildren<Animator>();
    }

    private void Update()
    {
        Animate();
    }

    private void Animate()
    {
        if (player.IsGrounded)
        {
            if (player.IsRunning)
            {
                PlayAnimation(Constants.Animations.Run);
                // TODO: Play different animations based on running speed
            }
            else
            {
                PlayAnimation(Constants.Animations.Idle);
            }
        }
        else
        {
            if (player.IsGoingUp && !player.IsOnTopOfPlayer)
            {
                PlayAnimation(Constants.Animations.JumpUp);
            }
            else if (player.IsFalling)
            {
                PlayAnimation(Constants.Animations.Falling);
            }
        }
    }

    void PlayAnimation(string newAnimation)
    {
        if (currentAnimation == newAnimation)
        {
            return;
        }

        animator.Play(newAnimation);
        currentAnimation = newAnimation;
    }
}
