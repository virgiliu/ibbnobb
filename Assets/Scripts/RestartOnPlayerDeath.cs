﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartOnPlayerDeath : MonoBehaviour
{
    public float secondsToRestart = 2f;

    int playersAlive = 2;


    private void OnEnable()
    {
        PlayerDeath.OnPlayerDies += PlayerDeath_OnPlayerDies;
    }

    private void OnDisable()
    {
        PlayerDeath.OnPlayerDies -= PlayerDeath_OnPlayerDies;
    }


    private void PlayerDeath_OnPlayerDies(GameObject player)
    {
        playersAlive--;

        if (playersAlive <= 0)
        {
            StartCoroutine(RestartCo());
        }
    }

    IEnumerator RestartCo()
    {
        yield return new WaitForSeconds(secondsToRestart);

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
