﻿using UnityEngine;

/// <summary>
/// Super simple local multiplayer input for 2 players.
/// If you add new axis, you need to follow the naming convention "Axis_Name{PlayerId}".
/// To get an axis name for a player, see GetAxisName().
/// </summary>
public class SimpleInput : MonoBehaviour
{
    [Range(0, 1)]
    public int playerId = 0;

    public float HorizontalAxis
    {
        get
        {
            return Input.GetAxisRaw(GetAxisName(Constants.Inputs.Horizontal));
        }
    }

    public bool WasJumpPressedThisFrame
    {
        get
        {
            return Input.GetButtonDown(GetAxisName(Constants.Inputs.Jump));
        }
    }

    public bool IsJumpHeldDown
    {
        get
        {
            return Input.GetButton(GetAxisName(Constants.Inputs.Jump));
        }
    }

    private string GetAxisName(string axisBaseName)
    {
        return $"{axisBaseName}{playerId}";
    }
}
