﻿using UnityEngine;

/// <summary>
/// This component allows the players to "fall" up or down
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class CustomPhysics : MonoBehaviour
{
    public Vector3 Gravity { get; private set; }

    private bool gravityFlipped = false;
    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        ApplyGravity();
    }

    public void FlipGravity()
    {
        gravityFlipped = !gravityFlipped;
    }

    private void ApplyGravity()
    {
        Gravity = Physics.gravity;

        if (gravityFlipped)
        {
            Gravity *= -1;
        }

        rb.AddForce(Gravity);
    }
}
