﻿using UnityEngine;

/// <summary>
/// Flips mesh orientation left/right.
/// Should be attached to the parent object of the player meshes.
/// </summary>
public class FlipPlayerMeshLeftRight : MonoBehaviour
{
    private PlayerController player;
    private bool facingRight;

    private void Start()
    {

        player = transform.parent.GetComponent<PlayerController>();

        if (player == null)
        {
            Debug.LogError($"Parent does not have a PlayerController.\nThe prefab structure is either wrong or you attached this script to the wrong object.");
        }
        else
        {
            facingRight = player.IsFacingRight;
        }
    }

    private void Update()
    {
        if (player != null)
        {
            if (player.IsFacingRight != facingRight)
            {
                FlipMesh();
            }
        }
    }

    private void FlipMesh()
    {
        transform.localScale = new Vector3(
            transform.localScale.x,
            transform.localScale.y,
            transform.localScale.z * -1
        );

        facingRight = player.IsFacingRight;
    }
}
